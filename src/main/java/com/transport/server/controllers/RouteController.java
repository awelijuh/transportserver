package com.transport.server.controllers;

import com.transport.server.controllers.mapper.RouteMapper;
import com.transport.server.controllers.resources.RouteResource;
import com.transport.server.dao.model.Route;
import com.transport.server.services.RouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("route")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RouteController {


    @NonNull
    private final RouteMapper routeMapper;

    @NonNull
    private final RouteService routeService;

    @GetMapping
    ResponseEntity<List<RouteResource>> getAll() {
        return ResponseEntity.ok(
                routeMapper.toResource(routeService.findAll())
        );
    }

    @GetMapping("{id}")
    ResponseEntity<RouteResource> getRoute(@PathVariable("id") Route route) {
        return ResponseEntity.ok(
                routeMapper.toResource(route)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<RouteResource> update(@PathVariable("id") Route route, @Valid @RequestBody RouteResource resource) {
        return ResponseEntity.ok(
                routeMapper.toResource(
                        routeService.save(
                                routeMapper.update(route, resource)
                        )
                )
        );
    }

    @PostMapping
    ResponseEntity<RouteResource> createRoute(@Valid @RequestBody RouteResource resource) {
        return ResponseEntity.ok(
                routeMapper.toResource(
                        routeService.save(
                                routeMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<RouteResource> deleteRoute(@PathVariable("id") Route route) {
        routeService.delete(route);
        return ResponseEntity.noContent().build();
    }

}
