package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.TransportResource;
import com.transport.server.dao.model.Driver;
import com.transport.server.dao.model.Transport;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class TransportMapper implements Mapper<Transport, TransportResource> {

    @NonNull
    private final ConversionService conversionService;

    @Override
    public Transport toModel(TransportResource resource) {
        return Transport.builder()
//                .driver(conversionService.convert(resource.getDriverId(), Driver.class))
                .maxSeats(resource.getMaxSeats())
                .build();
    }

    @Override
    public Transport update(Transport current, TransportResource resource) {
//        current.setDriver(conversionService.convert(resource.getDriverId(), Driver.class));
        current.setMaxSeats(resource.getMaxSeats());
        return current;
    }

    @Override
    public TransportResource toResource(Transport transport) {
        return TransportResource.builder()
//                .driverId(transport.getDriver().getId())
                .maxSeats(transport.getMaxSeats())
                .id(transport.getId())
                .build();
    }
}
