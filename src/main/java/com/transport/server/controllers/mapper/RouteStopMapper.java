package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.RouteStopResource;
import com.transport.server.dao.model.Route;
import com.transport.server.dao.model.RouteStop;
import com.transport.server.dao.model.Stop;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RouteStopMapper implements Mapper<RouteStop, RouteStopResource> {

    @NonNull
    ConversionService conversionService;

    @Override
    public RouteStop toModel(RouteStopResource routeStopResource) {
        return RouteStop.builder()
                .stop(conversionService.convert(routeStopResource.getStopId(), Stop.class))
                .route(conversionService.convert(routeStopResource.getRouteId(), Route.class))
                .pos(routeStopResource.getPos())
                .build();
    }

    @Override
    public RouteStop update(RouteStop current, RouteStopResource routeStopResource) {
        current.setPos(routeStopResource.getPos());
        current.setRoute(conversionService.convert(routeStopResource.getRouteId(), Route.class));
        current.setStop(conversionService.convert(routeStopResource.getStopId(), Stop.class));
        return current;
    }

    @Override
    public RouteStopResource toResource(RouteStop routeStop) {
        return RouteStopResource.builder()
                .pos(routeStop.getPos())
                .routeId(routeStop.getRoute().getId())
                .stopId(routeStop.getStop().getId())
                .id(routeStop.getId())
                .build();
    }
}
