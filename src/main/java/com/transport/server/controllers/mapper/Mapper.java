package com.transport.server.controllers.mapper;


import com.transport.server.dao.model.AbstractEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public interface Mapper<MODEL extends AbstractEntity, RESOURCE> extends ResourceMapper<MODEL, RESOURCE> {
    MODEL toModel(RESOURCE resource);

    MODEL update(MODEL current, RESOURCE resource);


    default List<MODEL> toModel(Iterable<RESOURCE> iterable) {
        return StreamSupport.stream(iterable.spliterator(), true)
                .map(this::toModel)
                .collect(Collectors.toList());
    }

}
