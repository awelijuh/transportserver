package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.MovementResource;
import com.transport.server.dao.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class MovementMapper implements Mapper<Movement, MovementResource> {

    @NonNull
    private final ConversionService conversionService;

    @Override
    public Movement toModel(MovementResource movementResource) {
        return Movement.builder()
                .driver(conversionService.convert(movementResource.getDriverId(), Driver.class))
                .route(conversionService.convert(movementResource.getRouteId(), Route.class))
                .transport(conversionService.convert(movementResource.getTransportId(), Transport.class))
                .movementStatus(new MovementStatus())
                .build();
    }

    @Override
    public Movement update(Movement current, MovementResource movementResource) {
        current.setDriver(conversionService.convert(movementResource.getDriverId(), Driver.class));
        current.setRoute(conversionService.convert(movementResource.getRouteId(), Route.class));
        current.setTransport(conversionService.convert(movementResource.getTransportId(), Transport.class));
        return current;
    }

    @Override
    public MovementResource toResource(Movement movement) {
        return MovementResource.builder()
                .driverId(movement.getDriver().getId())
                .routeId(movement.getRoute().getId())
                .transportId(movement.getTransport() != null ? movement.getTransport().getId() : null)
                .id(movement.getId())
                .movementStatusId(movement.getMovementStatus() != null ? movement.getMovementStatus().getId() : null)
                .build();
    }
}
