package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.MovementStatusResource;
import com.transport.server.dao.model.Movement;
import com.transport.server.dao.model.MovementStatus;
import com.transport.server.dao.model.Stop;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MovementStatusMapper implements Mapper<MovementStatus, MovementStatusResource> {

    @NonNull
    private final ConversionService conversionService;

    @NonNull
    private final LatLngMapper latLngMapper;

    @Override
    public MovementStatus toModel(MovementStatusResource movementStatusResource) {
        return MovementStatus.builder()
                .movement(conversionService.convert(movementStatusResource.getMovementId(), Movement.class))
                .numberOccupiedSeats(movementStatusResource.getNumberOccupiedSeats())
                .stop(conversionService.convert(movementStatusResource.getStopId(), Stop.class))
                .maxSeats(movementStatusResource.getMaxSeats())
                .latLng(latLngMapper.toModel(movementStatusResource.getLatLng()))
                .build();
    }

    @Override
    public MovementStatus update(MovementStatus current, MovementStatusResource movementStatusResource) {
//        current.setMovement(conversionService.convert(movementStatusResource.getMovementId(), Movement.class));
        current.setStop(conversionService.convert(movementStatusResource.getStopId(), Stop.class));
        current.setNumberOccupiedSeats(movementStatusResource.getNumberOccupiedSeats());
        current.setLatLng(latLngMapper.update(current.getLatLng(), movementStatusResource.getLatLng()));
        current.setMaxSeats(movementStatusResource.getMaxSeats());
        return current;
    }

    @Override
    public MovementStatusResource toResource(MovementStatus movementStatus) {
        return MovementStatusResource.builder()
                .movementId(movementStatus.getMovement().getId())
                .numberOccupiedSeats(movementStatus.getNumberOccupiedSeats())
                .stopId(movementStatus.getStop() != null ? movementStatus.getStop().getId() : null)
                .latLng(latLngMapper.toResource(movementStatus.getLatLng()))
                .maxSeats(movementStatus.getMaxSeats())
                .id(movementStatus.getId())
                .build();
    }
}
