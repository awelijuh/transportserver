package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.RequestResource;
import com.transport.server.dao.model.Request;
import com.transport.server.dao.model.RequestStatus;
import com.transport.server.dao.model.Route;
import com.transport.server.dao.model.Stop;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RequestMapper implements Mapper<Request, RequestResource> {

    @NonNull
    private final ConversionService conversionService;

    @Override
    public Request toModel(RequestResource requestResource) {
        return Request.builder()
                .destinationStop(conversionService.convert(requestResource.getDestinationStopId(), Stop.class))
                .route(conversionService.convert(requestResource.getRouteId(), Route.class))
                .requestStatus(conversionService.convert(requestResource.getRequestStatus(), RequestStatus.class))
                .stop(conversionService.convert(requestResource.getStopId(), Stop.class))
                .build();
    }

    @Override
    public Request update(Request current, RequestResource requestResource) {
        current.setDestinationStop(conversionService.convert(requestResource.getDestinationStopId(), Stop.class));
        current.setRoute(conversionService.convert(requestResource.getRouteId(), Route.class));
        current.setRequestStatus(RequestStatus.valueOf(requestResource.getRequestStatus()));
        current.setStop(conversionService.convert(requestResource.getStopId(), Stop.class));
        return current;
    }

    @Override
    public RequestResource toResource(Request request) {
        return RequestResource.builder()
                .creationTime(request.getCreationTime())
                .destinationStopId(request.getDestinationStop() != null ? request.getDestinationStop().getId() : null)
                .routeId(request.getRoute() != null ? request.getRoute().getId() : null)
                .requestStatus(request.getRequestStatus() != null ? String.valueOf(request.getRequestStatus()) : null)
                .stopId(request.getStop().getId())
                .id(request.getId())
                .build();
    }
}
