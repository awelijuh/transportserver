package com.transport.server.controllers.mapper;


import com.transport.server.dao.model.AbstractEntity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public interface ResourceMapper<MODEL extends AbstractEntity, RESOURCE> {
    public RESOURCE toResource(MODEL model);

    default public List<RESOURCE> toResource(Iterable<MODEL> iterable) {
        return StreamSupport.stream(iterable.spliterator(), true)
                .map(this::toResource)
                .collect(Collectors.toList());
    }


}
