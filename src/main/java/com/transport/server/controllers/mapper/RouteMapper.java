package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.RouteResource;
import com.transport.server.dao.model.Route;
import com.transport.server.dao.model.RouteStop;
import com.transport.server.dao.model.Stop;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RouteMapper implements Mapper<Route, RouteResource> {

    @NonNull
    ConversionService conversionService;

    List<RouteStop> toRouteStopList(List<Long> ids) {
        List<RouteStop> routeStops = new ArrayList<>();
        for (int i = 0; i < ids.size(); i++) {
            routeStops.add(
                    RouteStop
                            .builder()
                            .pos((long) i)
                            .stop(conversionService.convert(ids.get(i), Stop.class))
                            .build()
            );
        }
        return routeStops;
    }

    @Override
    public Route toModel(RouteResource resource) {
        return Route.builder()
                .name(resource.getName())
                .routeStop(toRouteStopList(resource.getStopIds()))
                .build();
    }

    @Override
    public Route update(Route current, RouteResource resource) {
        current.setName(resource.getName());
        current.setRouteStop(toRouteStopList(resource.getStopIds()));
        return current;
    }

    @Override
    public RouteResource toResource(Route route) {
        return RouteResource.builder()
                .id(route.getId())
                .name(route.getName())
                .stopIds(route.getRouteStop() != null ?
                        route.getRouteStop()
                                .stream()
                                .sorted(Comparator.comparingLong(RouteStop::getPos))
                                .map(e -> e.getStop().getId())
                                .collect(Collectors.toList())
                        : null)
                .build();
    }
}
