package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.LatLngResource;
import com.transport.server.dao.model.LatLng;
import org.springframework.stereotype.Component;

@Component
public class LatLngMapper implements Mapper<LatLng, LatLngResource> {
    @Override
    public LatLng toModel(LatLngResource resource) {
        if (resource == null) {
            return null;
        }
        return LatLng.builder()
                .latitude(resource.getLatitude())
                .longitude(resource.getLongitude())
                .build();
    }

    @Override
    public LatLng update(LatLng current, LatLngResource resource) {
        if (current == null) {
            return toModel(resource);
        }
        current.setLatitude(resource.getLatitude());
        current.setLongitude(resource.getLongitude());
        return current;
    }

    @Override
    public LatLngResource toResource(LatLng latLng) {
        if (latLng == null) {
            return null;
        }
        return LatLngResource.builder()
                .latitude(latLng.getLatitude())
                .longitude(latLng.getLongitude())
                .build();
    }
}
