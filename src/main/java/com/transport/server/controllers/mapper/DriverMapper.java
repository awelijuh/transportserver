package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.DriverResource;
import com.transport.server.dao.model.Driver;
import org.springframework.stereotype.Component;

@Component
public class DriverMapper implements Mapper<Driver, DriverResource> {
    @Override
    public Driver toModel(DriverResource driverResource) {
        return Driver.builder()
                .name(driverResource.getName())
                .email(driverResource.getEmail())
                .password(driverResource.getPassword())
                .build();
    }

    @Override
    public Driver update(Driver current, DriverResource driverResource) {
        current.setName(driverResource.getName());
        current.setEmail(driverResource.getEmail());
        current.setPassword(driverResource.getPassword());
        return current;
    }

    @Override
    public DriverResource toResource(Driver driver) {
        return DriverResource.builder()
                .email(driver.getEmail())
                .id(driver.getId())
                .name(driver.getName())
                .build();
    }
}
