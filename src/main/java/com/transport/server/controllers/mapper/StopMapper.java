package com.transport.server.controllers.mapper;

import com.transport.server.controllers.resources.StopResource;
import com.transport.server.dao.model.Stop;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class StopMapper implements Mapper<Stop, StopResource> {


    @NonNull
    LatLngMapper latLngMapper;

    @Override
    public Stop toModel(StopResource resource) {
        return Stop.builder()
                .key(resource.getKey())
                .name(resource.getName())
                .latLng(latLngMapper.toModel(resource.getLatLng()))
                .build();
    }

    @Override
    public Stop update(Stop current, StopResource resource) {
        current.setKey(resource.getKey());
        current.setLatLng(latLngMapper.update(current.getLatLng(), resource.getLatLng()));
        current.setName(resource.getName());
        return current;
    }

    @Override
    public StopResource toResource(Stop stop) {
        return StopResource.builder()
                .key(stop.getKey())
                .latLng(latLngMapper.toResource(stop.getLatLng()))
                .name(stop.getName())
                .id(stop.getId())
                .build();
    }
}
