package com.transport.server.controllers;

import com.transport.server.controllers.mapper.RouteStopMapper;
import com.transport.server.controllers.resources.RouteResource;
import com.transport.server.controllers.resources.RouteStopResource;
import com.transport.server.dao.model.RouteStop;
import com.transport.server.services.RouteStopService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("routeStop")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RouteStopController {

    @NonNull
    private final RouteStopMapper routeStopMapper;

    @NonNull
    private final RouteStopService routeStopService;

    @GetMapping
    ResponseEntity<List<RouteStopResource>> getAll() {
        return ResponseEntity.ok(
                routeStopMapper.toResource(routeStopService.findAll())
        );
    }

    @GetMapping("{id}")
    ResponseEntity<RouteStopResource> getRouteStop(@PathVariable("id") RouteStop routeStop) {
        return ResponseEntity.ok(
                routeStopMapper.toResource(routeStop)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<RouteStopResource> update(@PathVariable("id") RouteStop routeStop, @Valid @RequestBody RouteStopResource resource) {
        return ResponseEntity.ok(
                routeStopMapper.toResource(
                        routeStopService.save(
                                routeStopMapper.update(routeStop, resource)
                        )
                )
        );
    }

    @PostMapping
    ResponseEntity<RouteStopResource> createRouteStop(@Valid @RequestBody RouteStopResource resource) {
        return ResponseEntity.ok(
                routeStopMapper.toResource(
                        routeStopService.save(
                                routeStopMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<RouteStopResource> deleteRouteStop(@PathVariable("id") RouteStop routeStop) {
        routeStopService.delete(routeStop);
        return ResponseEntity.noContent().build();
    }
}
