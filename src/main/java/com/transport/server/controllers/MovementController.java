package com.transport.server.controllers;

import com.transport.server.controllers.mapper.MovementMapper;
import com.transport.server.controllers.resources.DriverResource;
import com.transport.server.controllers.resources.MovementResource;
import com.transport.server.dao.model.Movement;
import com.transport.server.services.MovementService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("movement")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class MovementController {


    @NonNull
    private final MovementMapper movementMapper;

    @NonNull
    private final MovementService movementService;

    @GetMapping("{id}")
    ResponseEntity<MovementResource> getMovement(@PathVariable("id") Movement movement) {
        return ResponseEntity.ok(
                movementMapper.toResource(movement)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<MovementResource> update(@PathVariable("id") Movement movement, @Valid @RequestBody MovementResource resource) {
        return ResponseEntity.ok(
                movementMapper.toResource(
                        movementService.save(
                                movementMapper.update(movement, resource)
                        )
                )
        );
    }

    @GetMapping
    ResponseEntity<List<MovementResource>> getAll() {
        return ResponseEntity.ok(
                movementMapper.toResource(movementService.findAll())
        );
    }

    @PostMapping
    ResponseEntity<MovementResource> createMovement(@Valid @RequestBody MovementResource resource) {
        return ResponseEntity.ok(
                movementMapper.toResource(
                        movementService.save(
                                movementMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<MovementResource> deleteMovement(@PathVariable("id") Movement movement) {
        movementService.delete(movement);
        return ResponseEntity.noContent().build();
    }

}
