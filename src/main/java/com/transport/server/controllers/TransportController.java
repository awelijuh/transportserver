package com.transport.server.controllers;

import com.transport.server.controllers.mapper.TransportMapper;
import com.transport.server.controllers.resources.TransportResource;
import com.transport.server.dao.model.Transport;
import com.transport.server.services.TransportService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("transport")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class TransportController {

    @NonNull
    private final TransportMapper transportMapper;

    @NonNull
    private final TransportService transportService;

    @GetMapping("{id}")
    ResponseEntity<TransportResource> getTransport(@PathVariable("id") Transport transport) {
        return ResponseEntity.ok(
                transportMapper.toResource(transport)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<TransportResource> update(@PathVariable("id") Transport transport, @Valid @RequestBody TransportResource resource) {
        return ResponseEntity.ok(
                transportMapper.toResource(
                        transportService.save(
                                transportMapper.update(transport, resource)
                        )
                )
        );
    }

    @PostMapping
    ResponseEntity<TransportResource> createTransport(@Valid @RequestBody TransportResource resource) {
        return ResponseEntity.ok(
                transportMapper.toResource(
                        transportService.save(
                                transportMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<TransportResource> deleteTransport(@PathVariable("id") Transport transport) {
        transportService.delete(transport);
        return ResponseEntity.noContent().build();
    }
}
