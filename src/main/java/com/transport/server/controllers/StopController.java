package com.transport.server.controllers;

import com.transport.server.controllers.mapper.StopMapper;
import com.transport.server.controllers.resources.StopResource;
import com.transport.server.dao.model.Stop;
import com.transport.server.services.StopService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("stop")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class StopController {

    @NonNull
    private final StopMapper stopMapper;

    @NonNull
    private final StopService stopService;

    @GetMapping
    ResponseEntity<List<StopResource>> getAll() {
        return ResponseEntity.ok(
                stopMapper.toResource(stopService.findAll())
        );
    }

    @GetMapping("{id}")
    ResponseEntity<StopResource> getStop(@PathVariable("id") Stop stop) {
        return ResponseEntity.ok(
                stopMapper.toResource(stop)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<StopResource> update(@PathVariable("id") Stop stop, @Valid @RequestBody StopResource resource) {
        return ResponseEntity.ok(
                stopMapper.toResource(
                        stopService.save(
                                stopMapper.update(stop, resource)
                        )
                )
        );
    }

    @PostMapping
    ResponseEntity<StopResource> createStop(@Valid @RequestBody StopResource resource) {
        return ResponseEntity.ok(
                stopMapper.toResource(
                        stopService.save(
                                stopMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<StopResource> deleteStop(@PathVariable("id") Stop stop) {
        stopService.delete(stop);
        return ResponseEntity.noContent().build();
    }
}
