package com.transport.server.controllers;

import com.transport.server.controllers.mapper.RequestMapper;
import com.transport.server.controllers.resources.RequestResource;
import com.transport.server.dao.model.Request;
import com.transport.server.services.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("request")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RequestController {

    @NonNull
    private final RequestMapper requestMapper;

    @NonNull
    private final RequestService requestService;

    @GetMapping
    ResponseEntity<List<RequestResource>> getAll() {
        return ResponseEntity.ok(
                requestMapper.toResource(
                        requestService.findAll()
                )
        );
    }

    @GetMapping("{id}")
    ResponseEntity<RequestResource> getRequest(@PathVariable("id") Request request) {
        return ResponseEntity.ok(
                requestMapper.toResource(request)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<RequestResource> update(@PathVariable("id") Request request, @Valid @RequestBody RequestResource resource) {
        return ResponseEntity.ok(
                requestMapper.toResource(
                        requestService.save(
                                requestMapper.update(request, resource)
                        )
                )
        );
    }

    @PostMapping
    ResponseEntity<RequestResource> createRequest(@Valid @RequestBody RequestResource resource) {
        return ResponseEntity.ok(
                requestMapper.toResource(
                        requestService.save(
                                requestMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<RequestResource> deleteRequest(@PathVariable("id") Request request) {
        requestService.delete(request);
        return ResponseEntity.noContent().build();
    }


}
