package com.transport.server.controllers;

import com.transport.server.controllers.mapper.DriverMapper;
import com.transport.server.controllers.resources.DriverResource;
import com.transport.server.dao.model.Driver;
import com.transport.server.services.DriverService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("driver")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class DriverController {

    @NonNull
    DriverMapper driverMapper;

    @NonNull
    DriverService driverService;


    @GetMapping
    ResponseEntity<List<DriverResource>> getAll() {
        return ResponseEntity.ok(
                driverMapper.toResource(driverService.findAll())
        );
    }

    @GetMapping("{id}")
    ResponseEntity<DriverResource> getDriver(@PathVariable("id") Driver driver) {
        return ResponseEntity.ok(
                driverMapper.toResource(driver)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<DriverResource> update(@PathVariable("id") Driver driver, @Valid @RequestBody DriverResource resource) {
        return ResponseEntity.ok(
                driverMapper.toResource(
                        driverService.save(
                                driverMapper.update(driver, resource)
                        )
                )
        );
    }

    @PostMapping
    ResponseEntity<DriverResource> createDriver(@Valid @RequestBody DriverResource resource) {
        return ResponseEntity.ok(
                driverMapper.toResource(
                        driverService.save(
                                driverMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<DriverResource> deleteDriver(@PathVariable("id") Driver driver) {
        driverService.delete(driver);
        return ResponseEntity.noContent().build();
    }

}
