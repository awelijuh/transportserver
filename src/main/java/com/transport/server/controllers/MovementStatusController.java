package com.transport.server.controllers;

import com.transport.server.controllers.mapper.MovementStatusMapper;
import com.transport.server.controllers.resources.MovementStatusResource;
import com.transport.server.dao.model.MovementStatus;
import com.transport.server.services.MovementStatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("movementStatus")
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class MovementStatusController {


    @NonNull
    private final MovementStatusMapper movementStatusMapper;

    @NonNull
    private final MovementStatusService movementStatusService;

    @GetMapping
    ResponseEntity<List<MovementStatusResource>> getAll() {
        return ResponseEntity.ok(
                movementStatusMapper.toResource(movementStatusService.findAll())
        );
    }

    @GetMapping("{id}")
    ResponseEntity<MovementStatusResource> getMovementStatus(@PathVariable("id") MovementStatus movementStatus) {
        return ResponseEntity.ok(
                movementStatusMapper.toResource(movementStatus)
        );
    }

    @PutMapping("{id}")
    ResponseEntity<MovementStatusResource> update(@PathVariable("id") MovementStatus movementStatus, @Valid @RequestBody MovementStatusResource resource) {
        return ResponseEntity.ok(
                movementStatusMapper.toResource(
                        movementStatusService.save(
                                movementStatusMapper.update(movementStatus, resource)
                        )
                )
        );
    }

    @PostMapping
    ResponseEntity<MovementStatusResource> createMovementStatus(@Valid @RequestBody MovementStatusResource resource) {
        return ResponseEntity.ok(
                movementStatusMapper.toResource(
                        movementStatusService.save(
                                movementStatusMapper.toModel(resource)
                        )
                )
        );
    }

    @DeleteMapping("{id}")
    ResponseEntity<MovementStatusResource> deleteMovementStatus(@PathVariable("id") MovementStatus movementStatus) {
        movementStatusService.delete(movementStatus);
        return ResponseEntity.noContent().build();
    }
    
}
