package com.transport.server.controllers.resources;

import com.transport.server.dao.model.LatLng;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Builder
public class RequestResource extends AbstractResource {

    private Long id;

    @NotNull
    private Long stopId;

    private Long destinationStopId;

    private Long routeId;

    private String requestStatus;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime creationTime;

}
