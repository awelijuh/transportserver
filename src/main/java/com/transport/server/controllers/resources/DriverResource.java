package com.transport.server.controllers.resources;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class DriverResource extends AbstractResource {

    private Long id;

    @NotNull
    @Size(min = 1, max = 200)
    private String name;

    @NotNull
    @Email
    @Size(min = 5, max = 200)
    private String email;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NotNull
    @Size(min = 6, max = 200)
    private String password;
}
