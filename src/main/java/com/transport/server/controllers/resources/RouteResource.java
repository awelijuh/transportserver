package com.transport.server.controllers.resources;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
public class RouteResource extends AbstractResource {

    @NotNull
    @Size(min = 1, max = 100)
    private String name;

    private List<Long> stopIds;

    private Long id;


}
