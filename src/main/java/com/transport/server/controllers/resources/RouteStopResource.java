package com.transport.server.controllers.resources;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class RouteStopResource extends AbstractResource {
    @NotNull
    private Long routeId;

    @NotNull
    private Long stopId;

    private Long pos;

    private Long id;

}
