package com.transport.server.controllers.resources;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class StopResource extends AbstractResource {
    @NotNull
    @Size(min = 1, max = 100)
    private String name;

    private String key;

    private LatLngResource latLng;

    private Long id;

}
