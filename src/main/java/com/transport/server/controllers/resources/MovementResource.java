package com.transport.server.controllers.resources;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class MovementResource extends AbstractResource {

    @NotNull
    private Long driverId;

    @NotNull
    private Long routeId;

    private Long transportId;

    private Long id;

    private Long movementStatusId;

}
