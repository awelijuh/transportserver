package com.transport.server.controllers.resources;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LatLngResource extends AbstractResource {

    Double latitude;

    Double longitude;

}
