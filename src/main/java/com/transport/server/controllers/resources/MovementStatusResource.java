package com.transport.server.controllers.resources;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MovementStatusResource extends AbstractResource {

    private Long id;

    private Long stopId;

    private Long numberOccupiedSeats;

    private Long maxSeats;

    private Long movementId;

    private LatLngResource latLng;
}
