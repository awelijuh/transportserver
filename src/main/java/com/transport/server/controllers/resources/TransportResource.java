package com.transport.server.controllers.resources;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class TransportResource extends AbstractResource {

    @NotNull
    private Long driverId;

    private Long maxSeats;

    private Long id;


}
