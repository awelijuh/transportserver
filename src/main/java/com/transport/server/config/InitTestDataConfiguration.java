package com.transport.server.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.transport.server.controllers.mapper.RouteMapper;
import com.transport.server.controllers.resources.RouteResource;
import com.transport.server.dao.DriverRepository;
import com.transport.server.dao.StopRepository;
import com.transport.server.dao.model.Driver;
import com.transport.server.dao.model.Stop;
import com.transport.server.services.RouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.lang.NonNull;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@Configuration
@RequiredArgsConstructor
public class InitTestDataConfiguration {

    @NonNull
    private final StopRepository stopRepository;

    @NonNull
    private final DriverRepository driverRepository;

    @NonNull
    private final RouteService routeService;

    @NonNull
    private final RouteMapper routeMapper;


    //    @Profile("initdb")
    @Bean
    void testData() throws IOException {
//        System.out.println(new File(".").getAbsolutePath());
//        File file = ResourceUtils.getFile("classpath:stops_data.json");
        ClassPathResource res = new ClassPathResource("stops_data.json");
        System.out.println(res.exists());
//        FileReader fileInputStream = new FileReader(res.getPath());
        InputStream inputStream = res.getInputStream();
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        String s = dataInputStream.readLine();

//        String s = bufferedReader.lines().collect(Collectors.joining());
        List<Stop> stops = new ObjectMapper().readValue(s, new TypeReference<List<Stop>>() {
        });
        stopRepository.saveAll(stops);

        driverRepository.save(
                Driver.builder()
                        .name("test driver")
                        .email("driver.test@test.test")
                        .password("12345678").build()
        );

        routeService.save(
                routeMapper.toModel(RouteResource.builder()
                        .name("test")
                        .stopIds(Arrays.asList(1L, 3L, 23L, 12L, 76L, 33L))
                        .build())
        );


    }
}
