package com.transport.server.dao;

import com.transport.server.dao.model.Request;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends CommonCrudRepository<Request> {
}
