package com.transport.server.dao;

import com.transport.server.dao.model.AbstractEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CommonCrudRepository<T extends AbstractEntity> extends CrudRepository<T, Long> {
}
