package com.transport.server.dao;

import com.transport.server.dao.model.RouteStop;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RouteStopRepository extends CommonCrudRepository<RouteStop> {

    List<RouteStop> findAllByRouteId(Long routeId);

}
