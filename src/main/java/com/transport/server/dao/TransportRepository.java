package com.transport.server.dao;

import com.transport.server.dao.model.Transport;
import org.springframework.stereotype.Repository;

@Repository
public interface TransportRepository extends CommonCrudRepository<Transport> {
}
