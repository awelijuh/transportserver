package com.transport.server.dao;

import com.transport.server.dao.model.Stop;
import org.springframework.stereotype.Repository;

@Repository
public interface StopRepository extends CommonCrudRepository<Stop> {
}
