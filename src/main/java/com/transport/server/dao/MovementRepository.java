package com.transport.server.dao;


import com.transport.server.dao.model.Movement;
import org.springframework.stereotype.Repository;

@Repository
public interface MovementRepository extends CommonCrudRepository<Movement> {
}
