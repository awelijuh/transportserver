package com.transport.server.dao;

import com.transport.server.dao.model.LatLng;
import org.springframework.stereotype.Repository;

@Repository
public interface LatLngRepository extends CommonCrudRepository<LatLng> {
}
