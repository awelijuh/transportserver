package com.transport.server.dao;

import com.transport.server.dao.model.Route;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends CommonCrudRepository<Route> {
}
