package com.transport.server.dao;

import com.transport.server.dao.model.Driver;
import org.springframework.stereotype.Repository;

@Repository
public interface DriverRepository extends CommonCrudRepository<Driver> {
}
