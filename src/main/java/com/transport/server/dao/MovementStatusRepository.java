package com.transport.server.dao;

import com.transport.server.dao.model.MovementStatus;

public interface MovementStatusRepository extends CommonCrudRepository<MovementStatus> {
}
