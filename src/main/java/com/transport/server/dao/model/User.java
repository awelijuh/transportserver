package com.transport.server.dao.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class User extends AbstractEntity {

    @Column(nullable = false)
    @Size(min = 5, max = 200)
    private String name;

    @Column(nullable = false)
    @Size(min = 5, max = 200)
    @Email
    private String email;

    @Column(nullable = false)
    @Size(min = 8, max = 200)
    private String password;
}
