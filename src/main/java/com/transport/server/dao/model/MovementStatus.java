package com.transport.server.dao.model;

import lombok.*;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class MovementStatus extends AbstractEntity {

    @ManyToOne
    @JoinColumn
    private Stop stop;

    @Column
    private Long maxSeats;

    @Column
    private Long numberOccupiedSeats;

    @OneToOne(mappedBy = "movementStatus")
    private Movement movement;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private LatLng latLng;

}
