package com.transport.server.dao.model;

public enum RequestStatus {
    ONGOING, DONE, CANCELLED
}
