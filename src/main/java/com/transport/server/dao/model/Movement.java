package com.transport.server.dao.model;

import lombok.*;

import javax.persistence.*;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Movement extends AbstractEntity {

    @ManyToOne(optional = false)
    @JoinColumn
    private Driver driver;

    @ManyToOne(optional = false)
    @JoinColumn
    private Route route;

    @ManyToOne
    @JoinColumn
    private Transport transport;

    @OneToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn
    private MovementStatus movementStatus;

}
