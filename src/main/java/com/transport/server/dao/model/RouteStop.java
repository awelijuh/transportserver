package com.transport.server.dao.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class RouteStop extends AbstractEntity {

    @ManyToOne(optional = false)
    @JoinColumn
    private Route route;

    @ManyToOne(optional = false)
    @JoinColumn
    private Stop stop;

    @Column
    private Long pos;


}
