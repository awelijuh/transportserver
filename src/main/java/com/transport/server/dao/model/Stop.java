package com.transport.server.dao.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Stop extends AbstractEntity {

    @Column(nullable = false)
    @Size(min = 1, max = 100)
    private String name;

    @Column
    private String key;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private LatLng latLng;

//    @OneToMany(mappedBy = "stop")
//    private List<MovementStatus> movementStatusList;
//
//    @OneToMany(mappedBy = "stop")
//    private List<Request> requests;
//
//    @OneToMany(mappedBy = "destinationStop")
//    private List<Request> destinationRequests;

}
