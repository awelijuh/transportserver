package com.transport.server.dao.model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class LatLng extends AbstractEntity {

    @Column
    Double latitude;

    @Column
    Double longitude;

}
