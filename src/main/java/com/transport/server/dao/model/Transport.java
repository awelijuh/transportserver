package com.transport.server.dao.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Transport extends AbstractEntity {

//    @ManyToOne(optional = false)
//    @JoinColumn
//    private Driver driver;

    private Long maxSeats;

}
