package com.transport.server.services.filter.parser;

import org.springframework.data.jpa.domain.Specification;

public interface Expression<T> {
    Specification<T> toSpecification();
}
