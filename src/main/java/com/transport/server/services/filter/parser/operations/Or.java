package com.transport.server.services.filter.parser.operations;

import org.springframework.data.jpa.domain.Specification;

public class Or<T> {

    Specification<T> toSpecification(Specification<T> specification1, Specification<T> specification2) {
        return specification1.or(specification2);
    }
}
