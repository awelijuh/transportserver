package com.transport.server.services.filter;

import com.transport.server.services.filter.parser.Expression;
import com.transport.server.services.filter.parser.ExpressionParser;
import org.springframework.data.jpa.domain.Specification;

public abstract class Filter<T> {

    private final Expression<T> expression;

    public Filter(String filterString) {
        this.expression = ExpressionParser.parse(filterString);
    }


    public Specification<T> toSpecification() {
        return expression.toSpecification();
    }

}
