package com.transport.server.services.filter.parser.operations;

import com.transport.server.services.filter.parser.Element;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;


@RequiredArgsConstructor
public abstract class BinaryOperator<T> {
    protected final Element<T> first;
    protected final Element<T> second;

    abstract Specification<T> toSpecification();

}
