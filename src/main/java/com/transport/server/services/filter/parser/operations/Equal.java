package com.transport.server.services.filter.parser.operations;

import com.transport.server.services.filter.parser.Element;
import com.transport.server.services.filter.parser.Field;
import com.transport.server.services.filter.parser.Value;
import org.springframework.data.jpa.domain.Specification;

public class Equal<T> extends BinaryOperator<T> {

    public Equal(Element<T> first, Element<T> second) {
        super(first, second);
    }

    Specification<T> toSpecification(Field field, Value value) {
        return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(field.toPath(root), value.convert(field));
    }

    Specification<T> toSpecification(Field field1, Field field2) {
        return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(field1.toPath(root), field2.toPath(root));
    }

    @Override
    Specification<T> toSpecification() {
        if (!(first instanceof Field)) {
            throw new RuntimeException();
        }
        if (second instanceof Field) {
            return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(((Field) first).toPath(root), ((Field) second).toPath(root));
        } else if (second instanceof Value) {
            return (Specification<T>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(((Field) first).toPath(root), ((Value) second).convert((Field) first));
        }
        throw new RuntimeException();
    }
}
