package com.transport.server.services;

import com.transport.server.dao.model.Route;
import com.transport.server.services.common.DeleteService;
import com.transport.server.services.common.FindAllService;
import com.transport.server.services.common.SaveService;

public interface RouteService extends SaveService<Route>, DeleteService<Route>, FindAllService<Route> {
}
