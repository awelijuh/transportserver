package com.transport.server.services.common;

public interface CommonService<REPOSITORY> {
    REPOSITORY getRepository();
}
