package com.transport.server.services.common;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.model.AbstractEntity;

import java.util.Optional;

public interface FindByIdService<MODEL extends AbstractEntity> extends CommonService<CommonCrudRepository<MODEL>> {

    default Optional<MODEL> findById(Long id) {
        return getRepository().findById(id);
    }
}
