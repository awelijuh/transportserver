package com.transport.server.services.common;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.model.AbstractEntity;

public interface SaveService<T extends AbstractEntity> extends CommonService<CommonCrudRepository<T>> {
    default T save(T t) {
        return getRepository().save(t);
    }
}
