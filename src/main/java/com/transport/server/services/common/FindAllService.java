package com.transport.server.services.common;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.model.AbstractEntity;

public interface FindAllService<T extends AbstractEntity> extends CommonService<CommonCrudRepository<T>> {
    default Iterable<T> findAll() {
        return getRepository().findAll();
    }
}
