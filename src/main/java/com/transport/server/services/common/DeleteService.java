package com.transport.server.services.common;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.model.AbstractEntity;

public interface DeleteService<MODEL extends AbstractEntity> extends CommonService<CommonCrudRepository<MODEL>> {

    default void delete(MODEL model) {
        getRepository().delete(model);
    }
}
