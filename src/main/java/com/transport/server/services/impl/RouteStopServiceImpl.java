package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.RouteStopRepository;
import com.transport.server.dao.model.RouteStop;
import com.transport.server.services.RouteStopService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RouteStopServiceImpl implements RouteStopService {

    @NonNull
    RouteStopRepository routeStopRepository;

    @Override
    public CommonCrudRepository<RouteStop> getRepository() {
        return routeStopRepository;
    }
}
