package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.TransportRepository;
import com.transport.server.dao.model.Transport;
import com.transport.server.services.TransportService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class TransportServiceImpl implements TransportService {

    @NonNull
    private final TransportRepository transportRepository;

    @Override
    public CommonCrudRepository<Transport> getRepository() {
        return transportRepository;
    }
}
