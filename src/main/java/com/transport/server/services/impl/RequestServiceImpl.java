package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.RequestRepository;
import com.transport.server.dao.model.Request;
import com.transport.server.services.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RequestServiceImpl implements RequestService {

    @NonNull
    private final RequestRepository requestRepository;

    @Override
    public CommonCrudRepository<Request> getRepository() {
        return requestRepository;
    }
}
