package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.DriverRepository;
import com.transport.server.dao.model.Driver;
import com.transport.server.services.DriverService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class DriverServiceImpl implements DriverService {

    @NonNull
    private final DriverRepository driverRepository;

    @Override
    public CommonCrudRepository<Driver> getRepository() {
        return driverRepository;
    }
}
