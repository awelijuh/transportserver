package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.RouteRepository;
import com.transport.server.dao.RouteStopRepository;
import com.transport.server.dao.model.Route;
import com.transport.server.dao.model.RouteStop;
import com.transport.server.services.RouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class RouteServiceImpl implements RouteService {

    @NonNull
    private final RouteRepository routeRepository;

    @NonNull
    private final RouteStopRepository routeStopRepository;

    @Override
    public CommonCrudRepository<Route> getRepository() {
        return routeRepository;
    }

    @Override
    @Transactional
    public Route save(Route route) {
        List<RouteStop> oldRouteStops = routeStopRepository.findAllByRouteId(route.getId());
        for (RouteStop routeStop : oldRouteStops) {
            routeStop.setRoute(null);
        }
        for (RouteStop routeStop : route.getRouteStop()) {
            routeStop.setRoute(route);
        }
        List<RouteStop> deleteRouteStop = new ArrayList<>();
        for (RouteStop routeStop : oldRouteStops) {
            if (routeStop.getRoute() == null) {
                deleteRouteStop.add(routeStop);
            }
        }
        routeStopRepository.deleteAll(deleteRouteStop);
        return routeRepository.save(route);
    }
}
