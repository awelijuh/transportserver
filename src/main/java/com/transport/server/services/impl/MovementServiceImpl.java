package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.MovementRepository;
import com.transport.server.dao.model.Movement;
import com.transport.server.services.MovementService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class MovementServiceImpl implements MovementService {

    @NonNull
    private final MovementRepository movementRepository;

    @Override
    public CommonCrudRepository<Movement> getRepository() {
        return movementRepository;
    }
}
