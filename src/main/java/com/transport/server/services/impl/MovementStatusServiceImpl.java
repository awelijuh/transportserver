package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.MovementStatusRepository;
import com.transport.server.dao.model.MovementStatus;
import com.transport.server.services.MovementStatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MovementStatusServiceImpl implements MovementStatusService {

    private final MovementStatusRepository repository;

    @Override
    public CommonCrudRepository<MovementStatus> getRepository() {
        return repository;
    }
}
