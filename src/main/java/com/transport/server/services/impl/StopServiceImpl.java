package com.transport.server.services.impl;

import com.transport.server.dao.CommonCrudRepository;
import com.transport.server.dao.StopRepository;
import com.transport.server.dao.model.Stop;
import com.transport.server.services.StopService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class StopServiceImpl implements StopService {

    @NonNull
    private final StopRepository stopRepository;

    @Override
    public CommonCrudRepository<Stop> getRepository() {
        return stopRepository;
    }

}
