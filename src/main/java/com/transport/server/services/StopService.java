package com.transport.server.services;

import com.transport.server.dao.model.Stop;
import com.transport.server.services.common.DeleteService;
import com.transport.server.services.common.FindAllService;
import com.transport.server.services.common.SaveService;

public interface StopService extends SaveService<Stop>, DeleteService<Stop>, FindAllService<Stop> {

}
