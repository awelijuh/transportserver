package com.transport.server.services;

import com.transport.server.dao.model.Driver;
import com.transport.server.services.common.DeleteService;
import com.transport.server.services.common.FindAllService;
import com.transport.server.services.common.SaveService;

public interface DriverService extends SaveService<Driver>, DeleteService<Driver>, FindAllService<Driver> {
}
