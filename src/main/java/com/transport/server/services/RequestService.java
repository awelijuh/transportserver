package com.transport.server.services;

import com.transport.server.dao.model.Request;
import com.transport.server.services.common.DeleteService;
import com.transport.server.services.common.FindAllService;
import com.transport.server.services.common.SaveService;

public interface RequestService extends SaveService<Request>, DeleteService<Request>, FindAllService<Request> {
}
