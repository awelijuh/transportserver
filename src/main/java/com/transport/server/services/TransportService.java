package com.transport.server.services;

import com.transport.server.dao.model.Transport;
import com.transport.server.services.common.DeleteService;
import com.transport.server.services.common.SaveService;

public interface TransportService extends SaveService<Transport>, DeleteService<Transport> {
}
