package com.transport.server.services;

import com.transport.server.dao.model.Movement;
import com.transport.server.services.common.DeleteService;
import com.transport.server.services.common.FindAllService;
import com.transport.server.services.common.SaveService;

public interface MovementService extends SaveService<Movement>, DeleteService<Movement>, FindAllService<Movement> {
}
