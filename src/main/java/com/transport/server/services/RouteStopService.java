package com.transport.server.services;

import com.transport.server.dao.model.RouteStop;
import com.transport.server.services.common.DeleteService;
import com.transport.server.services.common.FindAllService;
import com.transport.server.services.common.SaveService;

public interface RouteStopService extends SaveService<RouteStop>, DeleteService<RouteStop>, FindAllService<RouteStop> {
}
